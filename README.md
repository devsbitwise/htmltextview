# Android HtmlTextView Module
This module was cloned from [HtmlTextView](https://github.com/SufficientlySecure/html-textview) which is no longer maintain by its original owner.
It contains classes or frameworks that are commonly use when developing an Android project that uses `Spans` with `HtmlCompat`.
The objective is to accelerate the development by providing custom `TextView` that can accept supported HTML tags of Android platform
using `Spanned` and `ImageSpan` to render formatted contents and images with flexibility to handle click events.


## Tools and Frameworks
- **HtmlCompat** - processes HTML strings into displayable styled text
- **Spanned** - powerful markup objects that you can use to style text at the character or paragraph level
- **ImageSpan** - allows you to insert images into spans of text within a TextView


## Prerequisite
This module assumed that you have prior experience on using the following:

- **Version Catalog** - Required


## Installation
Add the following to your project level `build.gradle` dependencies block.
```kotlin
// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    alias(libs.plugins.android.application).apply(false)
    alias(libs.plugins.android.kotlin).apply(false)
}
```

Add the following to your app level `build.gradle` dependencies block.
```kotlin
dependencies {
    implementation(project(":HtmlTextView"))
}
```

Add the following to your `settings.gradle`.
```kotlin
include(":HtmlTextView")
```

On your default TOML file `libs.versions.toml` add the following dependencies.
```toml
[versions]
core-ktx = "1.13.1"
appcompat = "1.7.0"
annotation = "1.8.2"

lifecycle-runtime = "2.8.6"

kotlinx-coroutines = "1.9.0"

kotlin = "2.0.20"

[libraries]
# AndroidX Core
core-ktx = { module = "androidx.core:core-ktx", version.ref = "core-ktx" }
appcompat = { module = "androidx.appcompat:appcompat", version.ref = "appcompat" }
annotation = { module = "androidx.annotation:annotation", version.ref = "annotation" }

# AndroidX Lifecycle
lifecycle-runtime-ktx = { module = "androidx.lifecycle:lifecycle-runtime-ktx", version.ref = "lifecycle-runtime" }

# Coroutine
kotlinx-coroutines-core = { module = "org.jetbrains.kotlinx:kotlinx-coroutines-core", version.ref = "kotlinx-coroutines" }
kotlinx-coroutines-android = { module = "org.jetbrains.kotlinx:kotlinx-coroutines-android", version.ref = "kotlinx-coroutines" }

[plugins]
android-library = { id = "com.android.library" }
android-kotlin = { id = "org.jetbrains.kotlin.android", version.ref = "kotlin" }
```


## Usage
Add the custom `AppCompatTextView` in your layout.
```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.appcompat.widget.LinearLayoutCompat 
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">
        <org.sufficientlysecure.htmltextview.HtmlTextView
            android:id="@+id/description_html_txt_v"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:paddingHorizontal="@dimen/space_12"
            android:paddingTop="@dimen/space_48"
            android:paddingBottom="@dimen/space_12"
            android:text="@tools:sample/lorem/random"
            android:textSize="@dimen/textSize14" />
</androidx.appcompat.widget.LinearLayoutCompat>
```

This module was migrated to **Kotlin** and is using **Kotlin Coroutines** instead of the old `AsyncTask` with `InputStream` for fetching images.
```kotlin
class MainActivity : AppCompatActivity() {

    private val binding get() = _binding!!

    private var _binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val tableLinkSpan = DrawTableLinkSpan().apply {
            textColor = resToColor(R.color.colorAccentDark_Accent)
            tableLinkText = getString(R.string.table)
        }

        binding.descriptionHtmlTxtV.apply {

            // The HTML table(s) are individually passed through to the ClickableTableSpan implementation
            // presumably for a WebView Activity.
            setClickableTableSpan(ClickableTableSpanImpl())

            setDrawTableLinkSpan(tableLinkSpan)

            // Best to use indentation that matches screen density.
            val metrics = resources.displayMetrics

            setListIndentPx(metrics.density * 10)

            setOnClickATagListener { _, _, href ->
                openInWebView(href)
                true
            }

            setOnClickImageTagListener { _, src ->
                openInWebView(src)
                true
            }

            setHtml(
                data,
                HtmlImageGetter(lifecycleScope, resources, this, R.drawable.error_no_connection)
            )

        }

    }

    private fun openInWebView(url: String?) {

        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        // Do try catch in case there is error in parsing or launching the intent
        startActivity(intent)

    }

    inner class ClickableTableSpanImpl : ClickableTableSpan() {

        override fun newInstance(): ClickableTableSpan {
            // Do not convert this to anonymous object with `return this` as it will not create new instance
            // so in case where multiple table tags exist, only the last table tag will be clickable.
            return ClickableTableSpanImpl()
        }

        override fun onClick(widget: View) {
            // Create your own WebView Activity to render HTML table tags
            Intent(this@MainActivity, HtmlViewActivity::class.java).apply {
                putExtra(HtmlViewActivity.INTENT_TABLE_HTML, tableHtml)
                startActivity(this)
            }
        }

    }

}
```


|                                                                                                 |                                  Examples of rendered content                                   |                                                                                                 |
|:-----------------------------------------------------------------------------------------------:|:-----------------------------------------------------------------------------------------------:|-------------------------------------------------------------------------------------------------|
| ![Native Ad](https://bitbucket.org/devsbitwise/htmltextview/raw/master/docs/HtmlTextView_1.png) | ![Native Ad](https://bitbucket.org/devsbitwise/htmltextview/raw/master/docs/HtmlTextView_2.png) | ![Native Ad](https://bitbucket.org/devsbitwise/htmltextview/raw/master/docs/HtmlTextView_3.png) |  


Support for **Base64 encoded image** was added and side to side rendering of image was also improved, showing contents centered on both sides of the screen.


## Case Studies
This module is being used on the following projects:

- [**Cryptonian**](https://bitbucket.org/devsbitwise/cryptonian/src/master/)


## Notes
- In order to use the resources (R class) of this library when using AGP 8.x.x, set `android.nonTransitiveRClass` to `false` in `gradle.properties`.
- **Gradle 8.8.+** must be use when using this library which allows [Version Catalog plugin aliases without a version](https://docs.gradle.org/8.8/release-notes.html) such as `com.android.library`.


# License
Apache License v2

See LICENSE for full license text.