/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@file:Suppress("unused", "RedundantSuppression")

package org.sufficientlysecure.htmltextview

import android.text.Html.ImageGetter
import android.text.Spanned
import androidx.core.text.HtmlCompat

object HtmlFormatter {
    fun formatHtml(builder: HtmlFormatterBuilder): Spanned {
        return formatHtml(
            builder.html,
            builder.imageGetter,
            builder.clickableTableSpan,
            builder.drawTableLinkSpan,
            object : TagClickListenerProvider {

                override fun onClickATagListener(): OnClickATagListener? {
                    return builder.onClickATagListener
                }

                override fun onClickImageTagListener(): OnClickImageTagListener? {
                    return builder.onClickImageTagListener
                }

            },
            builder.indent,
            builder.isRemoveTrailingWhiteSpace
        )
    }

    fun formatHtml(
        html: String,
        imageGetter: ImageGetter?,
        clickableTableSpan: ClickableTableSpan?,
        drawTableLinkSpan: DrawTableLinkSpan?,
        tagClickListenerProvider: TagClickListenerProvider?,
        indent: Float,
        removeTrailingWhiteSpace: Boolean
    ): Spanned {
        var source = html
        val htmlTagHandler = HtmlTagHandler()
        htmlTagHandler.setClickableTableSpan(clickableTableSpan)
        htmlTagHandler.setDrawTableLinkSpan(drawTableLinkSpan)
        htmlTagHandler.setOnClickATagListenerProvider(tagClickListenerProvider)
        htmlTagHandler.setOnclickImageTagListenerProvider(tagClickListenerProvider)
        htmlTagHandler.setListIndentPx(indent)
        source = htmlTagHandler.overrideTags(source)
        return if (removeTrailingWhiteSpace) {
            removeHtmlBottomPadding(
                HtmlCompat.fromHtml(
                    source, HtmlCompat.FROM_HTML_MODE_LEGACY,
                    imageGetter,
                    WrapperContentHandler(htmlTagHandler)
                )
            )
        } else {
            HtmlCompat.fromHtml(
                source,
                HtmlCompat.FROM_HTML_MODE_LEGACY,
                imageGetter,
                WrapperContentHandler(htmlTagHandler)
            )
        }
    }

    /**
     * Html.fromHtml sometimes adds extra space at the bottom.
     * This methods removes this space again.
     * See https://github.com/SufficientlySecure/html-textview/issues/19
     */
    private fun removeHtmlBottomPadding(text: Spanned): Spanned {
        var spanned = text
        while (spanned.isNotEmpty() && spanned[spanned.length - 1] == '\n') {
            spanned = spanned.subSequence(0, spanned.length - 1) as Spanned
        }
        return spanned
    }

    interface TagClickListenerProvider {

        fun onClickATagListener(): OnClickATagListener?

        fun onClickImageTagListener(): OnClickImageTagListener?

    }
}