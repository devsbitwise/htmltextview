/*
 * Copyright (C) 2014-2016 Dominik Schürmann <dominik@schuermann.eu>
 * Copyright (C) 2013 Antarix Tandon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.sufficientlysecure.htmltextview

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.Html.ImageGetter
import android.util.Base64
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.HtmlCompat
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import androidx.lifecycle.LifecycleCoroutineScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.net.URL
import kotlin.math.roundToInt


/**
 * Custom ImageGetter for [HtmlCompat.fromHtml] which accepts Url or Base64 from img tag.
 * */

@SuppressWarnings("kotlin:S6310") // Static Coroutine Dispatchers here should be just fine and can be ignored by SonarLint
class HtmlImageGetter(
    private val scope: LifecycleCoroutineScope,
    private val res: Resources,
    private val htmlTextView: AppCompatTextView,
    @DrawableRes
    private val errorImage: Int,
    private val matchParent: Boolean = true
) : ImageGetter {

    override fun getDrawable(source: String): Drawable {
        val holder = BitmapDrawablePlaceHolder(res, null)

        // Image URL can be incorrect or no longer valid at some point which then returns a null Bitmap in `onSuccess`
        // catch such possible error here
        val handler = CoroutineExceptionHandler { _, _ ->
            if (errorImage != 0) {
                BitmapFactory.decodeResource(res, errorImage)?.let {
                    setDrawable(holder, it)
                }
            }
        }

        scope.launch(Dispatchers.IO + handler) {
            runCatching {

                if (source.matches(Regex("data:image.*base64.*"))) {
                    val decode = Base64.decode(
                        source.replace("data:image.*base64".toRegex(), ""),
                        Base64.DEFAULT
                    )
                    BitmapFactory.decodeByteArray(decode, 0, decode.size) // Image tag has Base64 data
                } else {
                    BitmapFactory.decodeStream(URL(source).openStream()) // Image tag has source URL
                }

            }
                .onSuccess { setDrawable(holder, it) }
                .onFailure {

                    if (errorImage != 0) {
                        BitmapFactory.decodeResource(res, errorImage)?.let {
                            setDrawable(holder, it)
                        }
                    }

                }
        }

        return holder
    }

    @SuppressWarnings("kotlin:S1656") // Can be ignored by SonarLint due to special use case on setText
    private fun setDrawable(holder: BitmapDrawablePlaceHolder, bitmap: Bitmap) {
        val drawable = BitmapDrawable(res, bitmap)

        val width: Int
        val height: Int

        val metrics = res.displayMetrics
        val displayWidth =
            metrics.widthPixels - (htmlTextView.paddingStart + htmlTextView.paddingEnd + htmlTextView.marginStart + htmlTextView.marginEnd) * 100 / 100

        val imageWidthScaled = (drawable.intrinsicWidth * metrics.density)
        val imageHeightScaled = (drawable.intrinsicHeight * metrics.density)

        // Scale up if matchParent is true
        // Scale down if matchParent is false
        if (matchParent || imageWidthScaled > displayWidth) {
            width = displayWidth
            height = (drawable.intrinsicHeight * width / drawable.intrinsicWidth)
        }
        else {
            height = imageHeightScaled.roundToInt()
            width = imageWidthScaled.roundToInt()
        }

        drawable.setBounds(0, 0, width, height)

        holder.setDrawable(drawable)
        holder.setBounds(0, 0, width, height)

        scope.launch(Dispatchers.Main) {
            // Set the text again to fix images overlapping text
            htmlTextView.text = htmlTextView.text
        }
    }

    internal class BitmapDrawablePlaceHolder(res: Resources, bitmap: Bitmap?) :
        BitmapDrawable(res, bitmap) {
        private var drawable: Drawable? = null

        override fun draw(canvas: Canvas) {
            drawable?.draw(canvas)
        }

        fun setDrawable(drawable: Drawable) {
            this.drawable = drawable
        }
    }
}