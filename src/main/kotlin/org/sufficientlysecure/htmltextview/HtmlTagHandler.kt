/*
 * Copyright (C) 2013-2015 Dominik Schürmann <dominik@schuermann.eu>
 * Copyright (C) 2013-2015 Juha Kuitunen
 * Copyright (C) 2013 Mohammed Lakkadshaw
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sufficientlysecure.htmltextview

import android.text.Editable
import android.text.Html
import android.text.Layout
import android.text.Spannable
import android.text.Spanned
import android.text.style.AlignmentSpan
import android.text.style.BulletSpan
import android.text.style.LeadingMarginSpan
import android.text.style.StrikethroughSpan
import android.text.style.TypefaceSpan
import android.text.style.URLSpan
import android.util.Log
import android.view.View
import org.sufficientlysecure.htmltextview.HtmlFormatter.TagClickListenerProvider
import org.xml.sax.Attributes
import java.util.Locale
import java.util.Stack
import kotlin.math.roundToInt

/**
 * Some parts of this code are based on android.text.Html
 */
class HtmlTagHandler : WrapperTagHandler {
    /**
     * Newer versions of the Android SDK's [Html.TagHandler] handles &lt;ul&gt; and &lt;li&gt;
     * tags itself which means they never get delegated to this class. We want to handle the tags
     * ourselves so before passing the string html into Html.fromHtml(), we can use this method to
     * replace the &lt;ul&gt; and &lt;li&gt; tags with tags of our own.
     *
     * @param html String containing HTML, for example: "**Hello world!**"
     * @return html with replaced  and  *  tags
     * @see [Specific Android SDK Commit](https://github.com/android/platform_frameworks_base/commit/8b36c0bbd1503c61c111feac939193c47f812190)
     */
    fun overrideTags(html: String?): String {
        var html1 = html ?: return ""
        html1 = "<$PLACEHOLDER_ITEM></$PLACEHOLDER_ITEM>$html1"
        html1 = html1.replace("<ul", "<$UNORDERED_LIST")
        html1 = html1.replace("</ul>", "</$UNORDERED_LIST>")
        html1 = html1.replace("<ol", "<$ORDERED_LIST")
        html1 = html1.replace("</ol>", "</$ORDERED_LIST>")
        html1 = html1.replace("<li", "<$LIST_ITEM")
        html1 = html1.replace("</li>", "</$LIST_ITEM>")
        html1 = html1.replace("<a", "<$A_ITEM")
        html1 = html1.replace("</a>", "</$A_ITEM>")
        return html1
    }

    /**
     * Keeps track of lists (ol, ul). On bottom of Stack is the outermost list
     * and on top of Stack is the most nested list
     */
    private var lists = Stack<String>()

    /**
     * Tracks indexes of ordered lists so that after a nested list ends
     * we can continue with correct index of outer list
     */
    private var olNextIndex = Stack<Int>()

    /**
     * Running HTML table string based off of the root table tag. Root table tag being the tag which
     * isn't embedded within any other table tag. Example:
     *
     * <table>
     * ...
     * <table>
     * ...
    </table> *
     * ...
    </table> *
     *
     */
    private var tableHtmlBuilder = StringBuilder()

    /**
     * Tells us which level of table tag we're on; ultimately used to find the root table tag.
     */
    private var tableTagLevel = 0
    private var clickableTableSpan: ClickableTableSpan? = null
    private var drawTableLinkSpan: DrawTableLinkSpan? = null
    private var onClickATagListenerProvider: TagClickListenerProvider? = null
    private var onClickImageTagListenerProvider: TagClickListenerProvider? = null

    private class Ul
    private class Ol
    private class A(val href: String?)
    private class Code
    private class Center
    private class Strike
    private class Table
    private class Tr
    private class Th
    private class Td

    @SuppressWarnings("kotlin:S3776") // The library was developed and maintained years ago :P, can be ignored by SonarLint
    override fun handleTag(
        opening: Boolean,
        tag: String,
        output: Editable?,
        attributes: Attributes?
    ): Boolean {
        if (opening) {
            // opening tag
            if (BuildConfig.DEBUG) {
                Log.d(HtmlTextView.TAG, "opening, output: " + output.toString())
            }

            when {
                (tag.equals(UNORDERED_LIST, ignoreCase = true)) -> {
                    lists.push(tag)
                }
                (tag.equals(ORDERED_LIST, ignoreCase = true)) -> {
                    lists.push(tag)
                    olNextIndex.push(1)
                }
                (tag.equals(LIST_ITEM, ignoreCase = true)) -> {

                    output?.let {

                        if (it.isNotEmpty() && it[it.length - 1] != '\n')
                            it.append("\n")

                    }

                    if (lists.isNotEmpty()) {
                        val parentList = lists.peek()
                        if (parentList.equals(ORDERED_LIST, ignoreCase = true)) {
                            start(output, Ol())
                            olNextIndex.push(olNextIndex.pop() + 1)
                        } else if (parentList.equals(UNORDERED_LIST, ignoreCase = true)) {
                            start(output, Ul())
                        }
                    }
                }
                (tag.equals(A_ITEM, ignoreCase = true)) -> {
                    val href = attributes?.getValue("href")
                    start(output, A(href))
                }
                (tag.equals("code", ignoreCase = true)) -> {
                    start(output, Code())
                }
                (tag.equals("center", ignoreCase = true)) -> {
                    start(output, Center())
                }
                (tag.equals("s", ignoreCase = true) || tag.equals("strike", ignoreCase = true)) -> {
                    start(output, Strike())
                }
                (tag.equals("table", ignoreCase = true)) -> {
                    start(output, Table())
                    if (tableTagLevel == 0) {
                        tableHtmlBuilder = StringBuilder()
                        // We need some text for the table to be replaced by the span because
                        // the other tags will remove their text when their text is extracted
                        output?.append("table placeholder")
                    }
                    tableTagLevel++
                }
                (tag.equals("tr", ignoreCase = true)) -> {
                    start(output, Tr())
                }
                (tag.equals("th", ignoreCase = true)) -> {
                    start(output, Th())
                }
                (tag.equals("td", ignoreCase = true)) -> {
                    start(output, Td())
                }
                else -> return false
            }
        } else {
            // closing tag
            if (BuildConfig.DEBUG) {
                Log.d(HtmlTextView.TAG, "closing, output: " + output.toString())
            }

            when {
                (tag.equals(UNORDERED_LIST, ignoreCase = true)) -> {
                    lists.pop()
                }
                (tag.equals(ORDERED_LIST, ignoreCase = true)) -> {
                    lists.pop()
                    olNextIndex.pop()
                }
                (tag.equals(LIST_ITEM, ignoreCase = true)) -> {
                    if (lists.isNotEmpty()) {
                        val listItemIndent =
                            if (userGivenIndent > -1) userGivenIndent * 2 else DEFAULT_LIST_ITEM_INDENT
                        if (lists.peek().equals(UNORDERED_LIST, ignoreCase = true)) {

                            output?.let {

                                if (it.isNotEmpty() && it[it.length - 1] != '\n')
                                    it.append("\n")

                            }

                            // Nested BulletSpans increases distance between bullet and text, so we must prevent it.
                            var indent = if (userGivenIndent > -1) userGivenIndent else DEFAULT_INDENT
                            val bullet =
                                if (userGivenIndent > -1) BulletSpan(userGivenIndent) else defaultBullet
                            if (lists.size > 1) {
                                indent -= bullet.getLeadingMargin(true)
                                if (lists.size > 2) {
                                    // This gets more complicated when we add a LeadingMarginSpan into the same line:
                                    // we have also counter it's effect to BulletSpan
                                    indent -= (lists.size - 2) * listItemIndent
                                }
                            }
                            val newBullet = BulletSpan(indent)
                            end(
                                output, Ul::class.java, false,
                                LeadingMarginSpan.Standard(listItemIndent * (lists.size - 1)),
                                newBullet
                            )
                        } else if (lists.peek().equals(ORDERED_LIST, ignoreCase = true)) {

                            output?.let {

                                if (it.isNotEmpty() && it[it.length - 1] != '\n')
                                    it.append("\n")

                            }

                            // Nested NumberSpans increases distance between number and text, so we must prevent it.
                            var indent = if (userGivenIndent > -1) userGivenIndent else DEFAULT_INDENT
                            val span = NumberSpan(indent, olNextIndex.lastElement() - 1)
                            if (lists.size > 1) {
                                indent -= span.getLeadingMargin(true)
                                if (lists.size > 2) {
                                    // As with BulletSpan, we need to compensate for the spacing after the number.
                                    indent -= (lists.size - 2) * listItemIndent
                                }
                            }
                            val numberSpan = NumberSpan(indent, olNextIndex.lastElement() - 1)
                            end(
                                output, Ol::class.java, false,
                                LeadingMarginSpan.Standard(listItemIndent * (lists.size - 1)),
                                numberSpan
                            )
                        }
                    }
                }
                (tag.equals(A_ITEM, ignoreCase = true)) -> {

                    val a = getLast(output, A::class.java)

                    output?.let {

                        val spanStart = it.getSpanStart(a)
                        val spanEnd = it.length
                        val href = if (a is A) a.href else null
                        val spannedText = it.subSequence(spanStart, spanEnd).toString()

                        end(it, A::class.java, false, object : URLSpan(href) {
                            override fun onClick(widget: View) {

                                onClickATagListenerProvider?.let { clickTag ->
                                    val clickConsumed = clickTag.onClickATagListener()
                                        ?.onAnchorTagClick(widget, spannedText, url) ?: false
                                    if (clickConsumed.not()) {
                                        super.onClick(widget)
                                    }

                                }

                            }
                        })

                    }

                }
                (tag.equals("code", ignoreCase = true)) -> {
                    end(output, Code::class.java, false, TypefaceSpan("monospace"))
                }
                (tag.equals("center", ignoreCase = true)) -> {

                    end(
                        output,
                        Center::class.java,
                        true,
                        AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER)
                    )

                }
                (tag.equals("s", ignoreCase = true) || tag.equals("strike", ignoreCase = true)) -> {
                    end(output, Strike::class.java, false, StrikethroughSpan())
                }
                (tag.equals("table", ignoreCase = true)) -> {
                    tableTagLevel--

                    // When we're back at the root-level table
                    if (tableTagLevel == 0) {
                        val tableHtml = tableHtmlBuilder.toString()

                        var myClickableTableSpan: ClickableTableSpan? = null
                        clickableTableSpan?.let {
                            myClickableTableSpan = it.newInstance()
                            myClickableTableSpan?.tableHtml = tableHtml
                        }

                        var myDrawTableLinkSpan: DrawTableLinkSpan? = null
                        drawTableLinkSpan?.let {
                            myDrawTableLinkSpan = it.newInstance()
                        }

                        end(
                            output,
                            Table::class.java,
                            false,
                            myDrawTableLinkSpan,
                            myClickableTableSpan
                        )

                    } else {
                        end(output, Table::class.java, false)
                    }
                }
                (tag.equals("tr", ignoreCase = true)) -> {
                    end(output, Tr::class.java, false)
                }
                (tag.equals("th", ignoreCase = true)) -> {
                    end(output, Th::class.java, false)
                }
                (tag.equals("td", ignoreCase = true)) -> {
                    end(output, Td::class.java, false)
                }
                else -> return false
            }
        }
        storeTableTags(opening, tag)
        return true
    }

    /**
     * If we're arriving at a table tag or are already within a table tag, then we should store it
     * the raw HTML for our ClickableTableSpan
     */
    private fun storeTableTags(opening: Boolean, tag: String) {
        if (tableTagLevel > 0 || tag.equals("table", ignoreCase = true)) {
            tableHtmlBuilder.append("<")
            if (opening.not()) {
                tableHtmlBuilder.append("/")
            }
            tableHtmlBuilder
                .append(tag.lowercase(Locale.getDefault()))
                .append(">")
        }
    }

    /**
     * Mark the opening tag by using private classes
     */
    private fun start(output: Editable?, mark: Any) {

        output?.let {

            val len = it.length

            it.setSpan(mark, len, len, Spannable.SPAN_MARK_MARK)

            if (BuildConfig.DEBUG) {
                Log.d(HtmlTextView.TAG, "len: $len")
            }

        }

    }

    /**
     * Modified from [android.text.Html]
     */
    private fun end(
        output: Editable?,
        kind: Class<*>,
        paragraphStyle: Boolean,
        vararg replaces: Any?
    ) {

        output?.let {

            val obj = getLast(it, kind)
            // start of the tag
            val where = it.getSpanStart(obj)
            // end of the tag
            val len = it.length

            // If we're in a table, then we need to store the raw HTML for later
            if (tableTagLevel > 0) {
                val extractedSpanText = extractSpanText(it, kind)
                tableHtmlBuilder.append(extractedSpanText)
            }
            it.removeSpan(obj)
            if (where != len) {
                var thisLen = len
                // paragraph styles like AlignmentSpan need to end with a new line!
                if (paragraphStyle) {
                    it.append("\n")
                    thisLen++
                }
                for (replace in replaces) {
                    it.setSpan(replace, where, thisLen, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
                }
                if (BuildConfig.DEBUG) {
                    Log.d(HtmlTextView.TAG, "where: $where")
                    Log.d(HtmlTextView.TAG, "thisLen: $thisLen")
                }
            }

        }

    }

    /**
     * @return the text contained within a span and deletes it from the output string
     */
    private fun extractSpanText(output: Editable?, kind: Class<*>): CharSequence {
        val obj = getLast(output, kind)

        output?.let {

            // start of the tag
            val where = it.getSpanStart(obj)
            // end of the tag
            val len = it.length
            val extractedSpanText = it.subSequence(where, len)
            it.delete(where, len)

            return extractedSpanText

        }

        return ""
    }

    // Util method for setting pixels.
    fun setListIndentPx(px: Float) {
        userGivenIndent = px.roundToInt()
    }

    fun setClickableTableSpan(clickableTableSpan: ClickableTableSpan?) {
        this.clickableTableSpan = clickableTableSpan
    }

    fun setDrawTableLinkSpan(drawTableLinkSpan: DrawTableLinkSpan?) {
        this.drawTableLinkSpan = drawTableLinkSpan
    }

    fun setOnClickATagListenerProvider(onClickATagListenerProvider: TagClickListenerProvider?) {
        this.onClickATagListenerProvider = onClickATagListenerProvider
    }

    fun setOnclickImageTagListenerProvider(onClickImageTagListenerProvider: TagClickListenerProvider?) {
        this.onClickImageTagListenerProvider = onClickImageTagListenerProvider
    }

    companion object {
        const val UNORDERED_LIST = "HTML_TEXTVIEW_ESCAPED_UL_TAG"
        const val ORDERED_LIST = "HTML_TEXTVIEW_ESCAPED_OL_TAG"
        const val LIST_ITEM = "HTML_TEXTVIEW_ESCAPED_LI_TAG"
        const val A_ITEM = "HTML_TEXTVIEW_ESCAPED_A_TAG"
        const val PLACEHOLDER_ITEM = "HTML_TEXTVIEW_ESCAPED_PLACEHOLDER"
        private var userGivenIndent = -1
        private const val DEFAULT_INDENT = 10
        private const val DEFAULT_LIST_ITEM_INDENT = DEFAULT_INDENT * 2
        private val defaultBullet = BulletSpan(DEFAULT_INDENT)

        /**
         * Get last marked position of a specific tag kind (private class)
         */
        private fun getLast(text: Editable?, kind: Class<*>): Any? {

            text?.let {

                val objs = it.getSpans(0, it.length, kind)
                if (objs.isNotEmpty()) {
                    for (i in objs.size downTo 1) {
                        if (it.getSpanFlags(objs[i - 1]) == Spannable.SPAN_MARK_MARK) {
                            return objs[i - 1]
                        }
                    }
                }

            }

            return null
        }
    }
}