/*
 * Copyright (C) 2013-2014 Dominik Schürmann <dominik@schuermann.eu>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@file:Suppress("MemberVisibilityCanBePrivate")

package org.sufficientlysecure.htmltextview

import android.content.Context
import android.text.Html.ImageGetter
import android.text.Spannable
import android.text.Spanned
import android.text.style.ImageSpan
import android.text.style.QuoteSpan
import android.text.style.URLSpan
import android.util.AttributeSet
import android.view.View
import androidx.annotation.RawRes
import androidx.core.content.ContextCompat
import java.io.InputStream
import java.util.Scanner

class HtmlTextView : JellyBeanSpanFixTextView {
    private var blockQuoteBackgroundColor = ContextCompat.getColor(context, android.R.color.transparent)
    private var blockQuoteStripColor = ContextCompat.getColor(context, android.R.color.holo_orange_dark)
    private var blockQuoteStripWidth = 10f
    private var blockQuoteGap = 20f
    private var clickableTableSpan: ClickableTableSpan? = null
    private var drawTableLinkSpan: DrawTableLinkSpan? = null
    private var onClickATagListener: OnClickATagListener? = null
    private var onClickImageTagListener: OnClickImageTagListener? = null
    private var indent = 24.0f // Default to 24px.
    private var removeTrailingWhiteSpace = true

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context,
        attrs,
        defStyle)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context) : super(context)

    /**
     * @see org.sufficientlysecure.htmltextview.HtmlTextView.setHtml
     */
    fun setHtml(@RawRes resId: Int) {
        setHtml(resId, null)
    }

    /**
     * @see org.sufficientlysecure.htmltextview.HtmlTextView.setHtml
     */
    fun setHtml(html: String) {
        setHtml(html, null)
    }

    /**
     * Loads HTML from a raw resource, i.e., a HTML file in res/raw/.
     * This allows translatable resource (e.g., res/raw-de/ for german).
     * The containing HTML is parsed to Android's Spannable format and then displayed.
     *
     * @param resId       for example: R.raw.help
     * @param imageGetter for fetching images. Possible ImageGetter provided by this library:
     * HtmlLocalImageGetter and HtmlRemoteImageGetter
     */
    fun setHtml(@RawRes resId: Int, imageGetter: ImageGetter?) {
        val inputStreamText = context.resources.openRawResource(resId)
        setHtml(convertStreamToString(inputStreamText), imageGetter)
    }

    /**
     * Parses String containing HTML to Android's Spannable format and displays it in this TextView.
     * Using the implementation of Html.ImageGetter provided.
     *
     * @param html        String containing HTML, for example: "**Hello world!**"
     * @param imageGetter for fetching images. Possible ImageGetter provided by this library:
     * HtmlLocalImageGetter and HtmlRemoteImageGetter
     */
    fun setHtml(html: String, imageGetter: ImageGetter?) {
        val styledText = HtmlFormatter.formatHtml(
            html, imageGetter, clickableTableSpan, drawTableLinkSpan,
            object : HtmlFormatter.TagClickListenerProvider {

                override fun onClickATagListener(): OnClickATagListener? {
                    return onClickATagListener
                }

                override fun onClickImageTagListener(): OnClickImageTagListener? {
                    return onClickImageTagListener
                }

            },
            indent,
            removeTrailingWhiteSpace
        )

        replaceQuoteSpans(styledText)

        val spannable = styledText as Spannable

        for (span in spannable.getSpans(0, spannable.length, ImageSpan::class.java)) {

            val flags: Int = spannable.getSpanFlags(span)
            val start: Int = spannable.getSpanStart(span)
            val end: Int = spannable.getSpanEnd(span)

            spannable.setSpan(object : URLSpan(span.source) {
                override fun onClick(widget: View) {

                    val clickConsumed =
                        onClickImageTagListener?.onImageTagClick(widget, url) ?: false

                    if (clickConsumed.not()) {
                        super.onClick(widget)
                    }

                }
            }, start, end, flags)

        }

        text = styledText

        // make links work
        movementMethod = LocalLinkMovementMethod.instance
    }

    fun setClickableTableSpan(clickableTableSpan: ClickableTableSpan?) {
        this.clickableTableSpan = clickableTableSpan
    }

    fun setDrawTableLinkSpan(drawTableLinkSpan: DrawTableLinkSpan?) {
        this.drawTableLinkSpan = drawTableLinkSpan
    }

    fun setOnClickATagListener(onClickATagListener: OnClickATagListener?) {
        this.onClickATagListener = onClickATagListener
    }

    fun setOnClickImageTagListener(onClickImageTagListener: OnClickImageTagListener?) {
        this.onClickImageTagListener = onClickImageTagListener
    }

    /**
     * Add ability to increase list item spacing. Useful for configuring spacing based on device
     * screen size. This applies to ordered and unordered lists.
     *
     * @param px pixels to indent.
     */
    fun setListIndentPx(px: Float) {
        indent = px
    }

    private fun replaceQuoteSpans(spanned: Spanned) {
        val spannable = spanned as Spannable
        val quoteSpans = spannable.getSpans(0, spannable.length - 1, QuoteSpan::class.java)
        for (quoteSpan in quoteSpans) {
            val start = spannable.getSpanStart(quoteSpan)
            val end = spannable.getSpanEnd(quoteSpan)
            val flags = spannable.getSpanFlags(quoteSpan)
            spannable.removeSpan(quoteSpan)
            spannable.setSpan(DesignQuoteSpan(
                blockQuoteBackgroundColor,
                blockQuoteStripColor,
                blockQuoteStripWidth,
                blockQuoteGap),
                start,
                end,
                flags)
        }
    }

    override fun scrollTo(x: Int, y: Int) {
        // Do nothing, prevent any scroll which usually happens when using emulator and trackpad
    }

    companion object {
        const val TAG = "HtmlTextView"
        /**
         * http://stackoverflow.com/questions/309424/read-convert-an-inputstream-to-a-string
         */
        private fun convertStreamToString(`is`: InputStream): String {
            val s = Scanner(`is`).useDelimiter("\\A")
            return if (s.hasNext()) s.next() else ""
        }
    }
}