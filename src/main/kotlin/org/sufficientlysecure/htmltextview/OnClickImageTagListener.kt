/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.sufficientlysecure.htmltextview

import android.view.View

/**
 * This listener can define what happens when the a tag is clicked
 */
fun interface OnClickImageTagListener {

    /**
     * Notifies of image tag click events.
     * @param widget - the [HtmlTextView] instance
     * @param src - the url of image src
     * @return indicates whether the click event has been handled
     */
    fun onImageTagClick(widget: View?, src: String?): Boolean

}